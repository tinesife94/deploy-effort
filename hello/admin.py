from django.contrib import admin

# Register your models here.
from .models import Stock, StockIndex

admin.site.register(Stock)
admin.site.register(StockIndex)
