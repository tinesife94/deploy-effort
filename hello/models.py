from django.db import models

# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField("date created", auto_now_add=True)

class StockIndex(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_list_stocks(self):
        try:
            return self.stock_list
        except AttributeError:
            self.stock_list = [s.symbol for s in self.stock_set.all()]
            return self.stock_list

class Stock(models.Model):
    symbol = models.CharField(max_length=8)
    name = models.CharField(max_length=100)
    s_type = models.CharField(max_length=10)
    indexes = models.ManyToManyField(StockIndex)

    def __str__(self):
        return self.symbol
